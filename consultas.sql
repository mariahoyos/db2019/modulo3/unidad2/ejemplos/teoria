﻿-- (1) Todos los datos de los usuarios que hayan alquilado coches

SELECT DISTINCT usuarios.* FROM usuarios JOIN alquileres ON usuarios.codigoUsuario = alquileres.usuario;

-- (2) Crear una vista para almacenar la consulta anterior

CREATE OR REPLACE VIEW consulta1 AS 
  SELECT DISTINCT usuarios.* 
    FROM usuarios JOIN alquileres 
    ON usuarios.codigoUsuario = alquileres.usuario;

-- (3) Ejecutar la vista

SELECT * FROM consulta1;

-- (4) Todos los datos de los usuarios de Santander

SELECT * FROM usuarios
  WHERE poblacion='Santander';

-- (5) Crear una vista para guardar la consulta anterior

CREATE OR REPLACE VIEW consulta3 AS
  SELECT * FROM usuarios
  WHERE poblacion='Santander';

-- (6) Ejecuta la vista consulta3

SELECT * FROM consulta3;

-- (7) insertar datos a través de la vista

INSERT INTO consulta3 VALUES(DEFAULT, 'Jorge','Isla','Hombre');

SELECT * FROM usuarios;

-- (8) Crear la vista con restricciones activas - Los checks se hacen con where en la tabla en la que estés

CREATE OR REPLACE VIEW consulta3 AS
  SELECT * FROM usuarios
  WHERE poblacion='Santander'
  WITH LOCAL CHECK OPTION;

-- (9) Vista que me saque los coches rojos con restricción activada

CREATE OR REPLACE VIEW consulta5 AS
  SELECT * FROM coches
  WHERE color='rojo'
  WITH LOCAL CHECK OPTION;

-- (10) Ejecutar la vista

SELECT * FROM consulta5;

-- (11) Insertar un dato utilizando la vista

INSERT INTO consulta5 VALUES(DEFAULT,'Saab','verde');

-- (12) Marca del coche que más veces se ha alquilado

SELECT coche, COUNT(*)n FROM alquileres
  GROUP BY coche;

SELECT MAX(n) FROM (
  SELECT coche, COUNT(*)n FROM alquileres
  GROUP BY coche
  )c1;

SELECT marca FROM (
  SELECT coche, COUNT(*)n FROM alquileres
  GROUP BY coche
  )c1 
  JOIN coches ON coche=codigoCoche 
    WHERE n = (SELECT MAX(n) FROM (
      SELECT coche, COUNT(*)n FROM alquileres
      GROUP BY coche
      )c1);

-- otra

-- c1: número de veces que se ha alquilado cada marca
SELECT marca, COUNT(*)numero FROM coches 
  JOIN alquileres ON coches.codigoCoche = alquileres.coche
  GROUP BY marca;

-- c2: el número máximo de alquileres por marca
SELECT MAX(numero)maximo FROM (
    SELECT marca, COUNT(*)numero FROM coches 
    JOIN alquileres ON coches.codigoCoche = alquileres.coche
    GROUP BY marca
  )c1;

-- opción join

SELECT marca 
  FROM (
    SELECT marca, COUNT(*)numero FROM coches 
      JOIN alquileres ON coches.codigoCoche = alquileres.coche
      GROUP BY marca
  )c2 
  JOIN 
  (
    SELECT MAX(numero)maximo FROM (SELECT marca, COUNT(*)numero FROM coches JOIN alquileres ON coches.codigoCoche = alquileres.coche
      GROUP BY marca)c1
  )c3  
  ON maximo=numero;

-- opcion having
SELECT marca FROM coches 
  JOIN alquileres ON coches.codigoCoche = alquileres.coche
  GROUP BY marca
  HAVING COUNT(*) = (
    SELECT MAX(numero)maximo FROM (
      SELECT marca, COUNT(*)numero FROM coches 
      JOIN alquileres ON coches.codigoCoche = alquileres.coche
      GROUP BY marca
    )c1
);

-- (7) Crear la consulta anterior como una vista

CREATE OR REPLACE VIEW consulta6 AS 
  SELECT marca 
  FROM (
    SELECT marca, COUNT(*)numero FROM coches 
      JOIN alquileres ON coches.codigoCoche = alquileres.coche
      GROUP BY marca
  )c2 
  JOIN 
  (
    SELECT MAX(numero)maximo FROM (SELECT marca, COUNT(*)numero FROM coches JOIN alquileres ON coches.codigoCoche = alquileres.coche
      GROUP BY marca)c1
  )c3  
  ON maximo=numero;

-- (8) 

CREATE OR REPLACE VIEW consulta7 AS
  SELECT marca FROM coches 
  JOIN alquileres ON coches.codigoCoche = alquileres.coche
  GROUP BY marca
  HAVING COUNT(*) = (
    SELECT MAX(numero)maximo FROM (
      SELECT marca, COUNT(*)numero FROM coches 
      JOIN alquileres ON coches.codigoCoche = alquileres.coche
      GROUP BY marca
    )c1
  );